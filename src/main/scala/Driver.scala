import sun.plugin2.message.GetAppletMessage

import scala.util.{Failure, Success}

/**
  * Created by ebogard on 6/11/17.
  */
object Driver {

  type MATRIX = Seq[Seq[Option[Move]]]

  def main(args: Array[String]): Unit = {
    val game = new Game
    val size = 3
    val depth = 5
    println(s"TicTacToe! Size = ${size}")
    val playerTeam = getPlayerTeam
    val board = game.init(3)
    play(board, X, playerTeam, game, depth)

  }

  def play(board: Board[MATRIX], turn: Move, player: Move, game: Game, depth: Int): Boolean = {
    println(game.string(board));
    if (turn.equals(player)) {
      val move = getPlayerPosition
      val newBoard = game.move(board, Position(move._1, move._2), turn)
      if (newBoard.isSuccess) {
        newBoard.get match {
          case x: Won[MATRIX] => { println(s"${x.winner} wins!"); println(game.string(x)); true }
          case x: Bad[MATRIX] => { println("It's a cat's game!"); println(game.string(x)); false }
          case x: Good[MATRIX] => { play(x, game.other(turn), player, game, depth) }
        }
      }
      else {
        println(s"Error: try again"); play(board, turn, player, game, depth)
      }
    }
    else {
      val newBoard = game.findMove(board, depth, game, turn)
      newBoard match {
        case x: Won[MATRIX] => { println(s"${x.winner} wins!"); println(game.string(x)); true }
        case x: Bad[MATRIX] => { println("It's a cat's game!"); println(game.string(x)); false }
        case x: Good[MATRIX] => { play(x, game.other(turn), player, game, depth) }
      }
    }
  }

  def getPlayerTeam: Move = {
    println("Choose your destiny! (X/O)")
    scala.io.StdIn.readLine() match {
      case "X" | "x" => X
      case "O" | "o" => O
      case _ => println("Invalid choice, try again."); getPlayerTeam
    }
  }

  def getPlayerPosition: (Int, Int) = {
    println("Enter the row and column separated by a comma! e.g. 0,1")
    val rc = scala.io.StdIn.readLine().split(',')
    if (rc.length != 2) { println("Bad. Try again."); getPlayerPosition }
    (rc.head.toInt, rc.last.toInt)
  }


}
